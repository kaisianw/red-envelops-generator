
# Red Envelops Generator



## Usage

```
const redEnvelops = generateRedEnvelops({numOfPpl: 5, amount: 10}) 
// redEnvelops = [2, 4.35, 0.6, 0.65, 2.4 ] = 10
``` 
|| Type | Required
|--|--|--|
|numOfPpl| Number | Yes
|amount| Number | Yes
|min| Number | No (Default = 0.1)





