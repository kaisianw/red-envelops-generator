import { GenerateRedEnvelopsParams } from './types';
import { formatAmount, getRandomArbitrary } from './utils';

export const DEFAULT_MIN = 0.1;
export const getDefaultMax = (amount: number) => amount / 2;

const generateRedEnvelops = ({ numOfPpl, amount, min = 0.1 }: GenerateRedEnvelopsParams) => {
  const max = getDefaultMax(amount);
  let remainingAmt = amount;
  const redEnvelops = [];

  // if total min amount is greater than given amount return []
  // if numOfPpl === 0 return []
  if (min * numOfPpl > amount || numOfPpl === 0) return [];

  // generate random amount for numOfPpl but exclude the last one
  for (let i = 0; i < numOfPpl - 1; i++) {
    const minPendingAmt = min * (numOfPpl - 1 - i);
    const maxAssignableAmt = remainingAmt - minPendingAmt;
    const realMax = maxAssignableAmt > max ? max : maxAssignableAmt;
    const randomAmt = getRandomArbitrary(min, realMax);
    const formattedRandomAmt = formatAmount(randomAmt);
    remainingAmt -= formattedRandomAmt;
    redEnvelops.push(formattedRandomAmt);
  }

  // push the remaining amount to the last envelop
  redEnvelops.push(formatAmount(remainingAmt));

  return redEnvelops;
};

export default generateRedEnvelops;
