export const formatAmount = (num: number) => {
  return parseFloat((Math.floor((num + 0.025) * 20) / 20).toFixed(2));
};

export const getRandomArbitrary = (min: number, max: number) => {
  return Math.random() * (max - min) + min;
};
