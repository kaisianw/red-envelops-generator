import { formatAmount, getRandomArbitrary } from '.';

describe('utils', () => {
  test('random amount should between min and max', () => {
    const minValue = 10;
    const maxValue = 20;

    const randomAmt = getRandomArbitrary(minValue, maxValue);
    expect(randomAmt).toBeLessThanOrEqual(maxValue);
    expect(randomAmt).toBeGreaterThanOrEqual(minValue);
  });

  test('formatAmount works as expected', () => {
    expect(formatAmount(1.1)).toEqual(1.1);
    expect(formatAmount(1.11)).toEqual(1.1);
    expect(formatAmount(1.12)).toEqual(1.1);
    expect(formatAmount(1.13)).toEqual(1.15);
    expect(formatAmount(1.14)).toEqual(1.15);
    expect(formatAmount(1.15)).toEqual(1.15);
    expect(formatAmount(1.16)).toEqual(1.15);
    expect(formatAmount(1.17)).toEqual(1.15);
    expect(formatAmount(1.18)).toEqual(1.2);
    expect(formatAmount(1.19)).toEqual(1.2);
  });
});
