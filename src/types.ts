export type GenerateRedEnvelopsParams = {
  numOfPpl: number;
  amount: number;
  min?: number;
};
