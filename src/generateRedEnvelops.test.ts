import generateRedEnvelops, { DEFAULT_MIN, getDefaultMax } from './generateRedEnvelops';
import { GenerateRedEnvelopsParams } from './types';

describe('generate red envelops', () => {
  const validParams = [
    {
      numOfPpl: 10,
      amount: 5,
    },
    {
      numOfPpl: 20,
      amount: 5,
    },
    {
      numOfPpl: 10,
      amount: 10,
    },
    {
      numOfPpl: 1,
      amount: 1,
      min: 1,
    },
    {
      numOfPpl: 100,
      amount: 5000,
      min: 2,
    },
    {
      numOfPpl: 10,
      amount: 10,
      min: 1,
    },
    {
      numOfPpl: 100,
      amount: 5000,
      min: 0,
    },
  ];

  const invalidParams = [
    {
      numOfPpl: 100,
      amount: 5,
    },
    {
      numOfPpl: 1,
      amount: 1,
      min: 2,
    },
    {
      numOfPpl: 10000,
      amount: 0,
    },
    {
      numOfPpl: 10,
      amount: 1,
      min: 10,
    },
    {
      numOfPpl: 999,
      amount: 50,
      min: 999,
    },
    {
      numOfPpl: 0,
      amount: 10,
      min: 1,
    },
  ];

  const generateRedEnvelopsGroups = (paramsArr: GenerateRedEnvelopsParams[]) => {
    return paramsArr.map((params) => {
      const redEnvelops = generateRedEnvelops(params);
      return redEnvelops;
    });
  };

  const redEnvelopsGroups = generateRedEnvelopsGroups(validParams);
  const invalidRedEnvelopsGroups = generateRedEnvelopsGroups(invalidParams);

  test('generated envelops equals to given nums of people', () => {
    redEnvelopsGroups.forEach((group, idx) => {
      const { numOfPpl } = validParams[idx];
      expect(group.length).toEqual(numOfPpl);
    });
  });

  test('generated envelops total amount equals to given amount', () => {
    redEnvelopsGroups.forEach((group, idx) => {
      const { amount } = validParams[idx];
      const totalAmount = group.reduce((total, env) => (total += env), 0);
      expect(totalAmount).toBeCloseTo(amount);
    });
  });

  test('each generated envelop amount should between min and max', () => {
    redEnvelopsGroups.forEach((group, idx) => {
      const { min = DEFAULT_MIN, amount, numOfPpl } = validParams[idx];
      group.forEach((envelopAmt) => {
        if (numOfPpl > 1) expect(envelopAmt).toBeLessThanOrEqual(getDefaultMax(amount));
        expect(envelopAmt).toBeGreaterThanOrEqual(min);
      });
    });
  });

  test('return empty array if the given amount/min is invalid', () => {
    invalidRedEnvelopsGroups.forEach((group) => {
      expect(group.length).toEqual(0);
    });
  });
});
